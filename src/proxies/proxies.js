import H2o2 from 'h2o2';
import { Server } from 'hapi';

const server = new Server();

server.connection({
  host: 'localhost',
  port: Number(process.argv[2] || 8080),
});

server.register(H2o2, (error) => {
  if (error) {
    throw error;
  }
});

server.route({
  method: 'GET',
  path: '/proxy',
  handler: {
    proxy: {
      host: 'localhost',
      port: 65535,
    },
  },
});

server.start(() => console.log('Server running at:', server.info.uri));
