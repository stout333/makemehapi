import { Server } from 'hapi';

const server = new Server();

server.connection({
  host: 'localhost',
  port: Number(process.argv[2] || 8080),
});

server.route({ path: '/',
  method: 'GET',
  handler: (request, reply) => {
    reply(null, 'Hello hapi');
  },
});

server.start(() => console.log('Server running at:', server.info.uri));
