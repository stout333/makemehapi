import { Server } from 'hapi';
import Joi from 'joi';

const server = new Server();

server.connection({
  host: 'localhost',
  port: Number(process.argv[2] || 8080),
});

server.route({
  method: 'POST',
  path: '/login',
  handler: (request, reply) => {
    reply('login successful');
  },
  config: {
    validate: {
      payload: Joi.object({
        isGuest: Joi.boolean().required(),
        username: Joi.string().when('isGuest', { is: false, then: Joi.required() }),
        accessToken: Joi.string().alphanum(),
        password: Joi.string().alphanum(),
      })
      .options({ allowUnknown: true })
      .without('password', 'accessToken'),
    },
  },
});

server.start(() => console.log('Server running at:', server.info.uri));
