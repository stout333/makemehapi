import { Server } from 'hapi';
import HapiAuthBasic from 'hapi-auth-basic';

const server = new Server();

const validate = (request, username, password, callback) => {
  if (username === 'hapi' && password === 'auth') {
    return callback(null, true, {});
  }
  return callback(null, false);
};

server.connection({
  host: 'localhost',
  port: Number(process.argv[2] || 8080),
});

server.register(HapiAuthBasic, (error) => {
  if (error) {
    throw error;
  }
});

server.auth.strategy('simple', 'basic', { validateFunc: validate });

server.route({
  path: '/',
  method: 'GET',
  config: {
    auth: 'simple',
  },
  handler: (request, reply) => {
    reply();
  },
});

server.start(() => console.log('Server running at:', server.info.uri));
