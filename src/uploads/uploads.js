import { Server } from 'hapi';

const server = new Server();

server.connection({
  host: 'localhost',
  port: Number(process.argv[2] || 8080),
});

server.route({
  method: 'POST',
  path: '/upload',
  config: {
    payload: {
      output: 'stream',
      parse: true,
    },
  },
  handler: (request, reply) => {
    let body = '';

    request.payload.file.on('data', (data) => {
      body += data;
    });

    request.payload.file.on('end', () => {
      reply(JSON.stringify({
        description: request.payload.description,
        file: {
          data: body,
          filename: request.payload.file.hapi.filename,
          headers: request.payload.file.hapi.headers,
        },
      }));
      console.log(body);
    });
  },
});

server.start(() => console.log('Server running at:', server.info.uri));
