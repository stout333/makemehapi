import { Server } from 'hapi';

const server = new Server();

server.connection({
  host: 'localhost',
  port: Number(process.argv[2] || 8080),
});

server.route({
  method: 'GET',
  path: '/{name}',
  handler: (request, reply) => {
    reply(`Hello ${encodeURIComponent(request.params.name)}`);
  },
});

server.start(() => console.log('Server running at:', server.info.uri));
