import Path from 'path';
import Handlebars from 'handlebars';
import { Server } from 'hapi';
import Vision from 'vision';

const server = new Server();

server.connection({
  host: 'localhost',
  port: Number(process.argv[2] || 8080),
});

server.register(Vision, (error) => {
  if (error) {
    throw error;
  }
});

server.route({
  method: 'GET',
  path: '/',
  handler: {
    view: '../../public/helping/index.html',
  },
});

server.views({
  allowInsecureAccess: true,
  engines: {
    html: Handlebars,
  },
  helpersPath: Path.join(__dirname, 'helpers'),
  path: __dirname,
});

server.start(() => console.log('Server running at:', server.info.uri));
