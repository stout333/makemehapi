import { Server } from 'hapi';

const server = new Server();

server.connection({
  host: 'localhost',
  port: Number(process.argv[2] || 8080),
});

server.route({
  path: '/set-cookie',
  method: 'GET',
  config: {
    state: {
      parse: true,
      failAction: 'log',
    },
  },
  handler: (request, reply) => {
    const stateOptions = {
      domain: 'localhost',
      encoding: 'base64json',
      isHttpOnly: false,
      isSameSite: false,
      isSecure: false,
      path: '/',
      ttl: 10,
    };
    reply().state('session', { key: 'makemehapi' }, stateOptions);
  },
});

server.route({
  path: '/check-cookie',
  method: 'GET',
  handler: (request, reply) => {
    const session = request.state.session;
    if (session) {
      reply({ user: 'hapi' });
    } else {
      reply().statusCode(401);
    }
  },
});

server.start(() => console.log('Server running at:', server.info.uri));
