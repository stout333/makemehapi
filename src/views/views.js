import Vision from 'vision';
import Handlebars from 'handlebars';
import { Server } from 'hapi';

const server = new Server();

server.connection({
  host: 'localhost',
  port: Number(process.argv[2] || 8080),
});

server.register(Vision, (error) => {
  if (error) {
    throw error;
  }
});

server.route({
  method: 'GET',
  path: '/',
  handler: {
    view: '../../public/views/index.html',
  },
});

server.views({
  allowInsecureAccess: true,
  engines: {
    html: Handlebars,
  },
  path: __dirname,
});

server.start(() => console.log('Server running at:', server.info.uri));
