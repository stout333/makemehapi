import Path from 'path';
import Inert from 'inert';
import { Server } from 'hapi';

const server = new Server({
  connections: {
    routes: {
      files: {
        relativeTo: __dirname,
      },
    },
  },
});

server.connection({
  host: 'localhost',
  port: Number(process.argv[2] || 8080),
});

server.register(Inert, (error) => {
  if (error) {
    throw error;
  }
});

server.route({
  method: 'GET',
  path: '/',
  handler: (request, reply) => {
    reply.file('../../public/handling/index.html', { confine: false });
  },
});

server.start(() => console.log('Server running at:', server.info.uri));
