import { Server } from 'hapi';
import Joi from 'joi';

const server = new Server();

server.connection({
  host: 'localhost',
  port: Number(process.argv[2] || 8080),
});

server.route({
  method: 'GET',
  path: '/chickens/{breed}',
  handler: (request, reply) => {
    reply(request.params.breed);
  },
  config: {
    validate: {
      params: {
        breed: Joi.string().required(),
      },
    },
  },
});

server.start(() => console.log('Server running at:', server.info.uri));
