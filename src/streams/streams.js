import Fs from 'fs';
import Path from 'path';
import { Server } from 'hapi';
import Rot13 from 'rot13-transform';

const server = new Server();

server.connection({
  host: 'localhost',
  port: Number(process.argv[2] || 8080),
});

server.route({
  method: 'GET',
  path: '/',
  handler: (request, reply) => {
    const filePath = Path.join(__dirname, '../../public/streams/index.txt');
    reply(null, Fs.createReadStream(filePath).pipe(Rot13()));
  },
});

server.start(() => console.log('Server running at:', server.info.uri));
